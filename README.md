# Projektliste

Was man noch so lles tun könnte

Hier werden Projektideen gesammelt und diskutiert, die wir im Lab realisieren
wollen. Für die Idee ist es vorrangig egal wie teuer oder gefährlich sie ist.

Ideen werden als Issue im Issue Tracker dieses Projekts gesammelt. Wird ein
Projekt dann umgesetzt, bekommt es ein eigenes Repository hier in der Gruppe
und das Issue wird geschlossen.